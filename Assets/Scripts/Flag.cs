using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Flag : MonoBehaviour
{
    [SerializeField] string _sceneName;

    void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.GetComponent<Player>();
        if (player != null)
            return;

        var animator = GetComponent<Animator>();
        animator.SetTrigger("wave");
        // this's to active the flag's animation whenever the Player obj to collision.
        StartCoroutine(LoadAfterDelay());
    }

    IEnumerator LoadAfterDelay()
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(_sceneName); 
        // And this's to jump into another scene/ level.
    }
}