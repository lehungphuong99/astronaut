using System.Collections;
using Unity.VisualScripting;
using UnityEngine;

public class Slime : MonoBehaviour
{

    [SerializeField] Transform _leftSense;
    [SerializeField] Transform _rightSense;
    [SerializeField] float _direction = -1;
    [SerializeField] Sprite _deadSprite;

    Rigidbody2D _rigidBody2D;

    void Start()
    {
        _rigidBody2D = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        _rigidBody2D.velocity = new Vector2(_direction, _rigidBody2D.velocity.y);

        if (_direction < 0)
        {
            sensor(_leftSense);
        }

        else
        {
            sensor(_rightSense);
        }
    }

    private void sensor(Transform sense)
    {
        Debug.DrawRay(sense.position, Vector2.down * 1f, Color.green);
        var leftSensehit = Physics2D.Raycast(sense.position, Vector2.down, 0.1f
        );
        if (leftSensehit.collider == null)
            Turnaround();

        Debug.DrawRay(sense.position, new Vector2(_direction, 0) * 1f, Color.green);
        var rightSensehit = Physics2D.Raycast(sense.position, new Vector2(_direction, 0), 0.1f
        );
        if (rightSensehit.collider != null)
            Turnaround();
    }

    void Turnaround()
    {
        _direction *= -1;
        var spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.flipX = _direction > 0;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        var player = collision.collider.GetComponent<Player>();
        if (player == null)
            return;

        var contact = collision.contacts[0];
        Vector2 normal = contact.normal;
        Debug.Log($"Normal = {normal}");

        if (normal.y <= -0.7)
            StartCoroutine(Die());
        else
            player.ResetToStart();

    }

    IEnumerator Die()
    {
        Destroy(gameObject);
        var spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = _deadSprite;
        GetComponent<Collider2D>().enabled = false;
        GetComponent<Animator>().enabled = false;
        enabled = false;
        GetComponent<Rigidbody2D>().simulated = false;
    

    float Gcolor = 1;

        while (Gcolor > 0)
        {
            yield return null;
            Gcolor -= Time.deltaTime;
            spriteRenderer.color = new Color(1, Gcolor, 1, 1);
        }
    }    

}
