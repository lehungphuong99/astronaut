using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollapseLands : MonoBehaviour
{
    [SerializeField] float _fallSpeed = 9;
    [Range(0.1f, 4)][SerializeField] float _fallAftersecond = 4;
    [SerializeField] float _timeShake;    
    [Range(0.01f, 0.5f)][SerializeField] float _horizontalIndex = 0.5f;
    [Range(0.01f, 0.5f)][SerializeField] float _verticalIndex = 0.5f;
    [Tooltip("when there're no player on flatform")]
    [SerializeField] bool _resetTimeShake;

    Vector3 _landpos;
    public bool Iscolliding;
    HashSet<Player> PlayersInLand = new HashSet<Player>();
    bool _isfalling;
    
    void Start()
    {
        _landpos = transform.position;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.GetComponent<Player>();
        if (player == null)
            return;
        
        PlayersInLand.Add(player);
        Iscolliding = true;

        StartCoroutine(Landfall());
    }

    IEnumerator Landfall()
    {
        Debug.Log("firststep");
        yield return new WaitForSeconds(0.3f);  //readytoshake
        Debug.Log("secondstep");

        //float _timeShake = 0;
        while (_timeShake < _fallAftersecond)
        {
            float horizontal = UnityEngine.Random.Range( -_horizontalIndex, _horizontalIndex);
            float vertical = UnityEngine.Random.Range( -_verticalIndex, _verticalIndex);
            transform.position = _landpos + new Vector3(horizontal, vertical);
            float delayToFall = UnityEngine.Random.Range(0.01f, 0.1f);
            yield return new WaitForSeconds(delayToFall);

            _timeShake += delayToFall;
        }
        
        yield return new WaitForSeconds(0.5f);  // shaking
        
        Debug.Log("falling");
        _isfalling = true;

        var colliders = GetComponents<Collider2D>();
        foreach (var collider in colliders)
        {
            collider.enabled = false;
        }
        
        
        float timeToFall = 0;
        while (timeToFall < 3f)
        {
            transform.position += Vector3.down * Time.deltaTime * _fallSpeed;
            timeToFall += Time.deltaTime;
            yield return null;
        }
        
        Destroy(gameObject);
    }
    void OnTriggerExit2D(Collider2D collision)
    {
        if (_isfalling)
            return;
        
        var player = collision.GetComponent<Player>();
        if (player == null)
            return;
        
        PlayersInLand.Remove(player);
        if (PlayersInLand.Count == 0)
        {
            Iscolliding = false;
            StopCoroutine("_landfall");
            
            if (_resetTimeShake)
                _timeShake = 0;
        }
    }
}
