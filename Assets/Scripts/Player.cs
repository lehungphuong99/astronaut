using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;
using UnityEngine.UIElements;

public class Player : MonoBehaviour
{
    [Header("movement")]
    [SerializeField] float _speed = 7;
    [SerializeField] float _slipFactor = 1;
    [Header("Jump")]
    [SerializeField] float _jumpVelocity = 6;
    [SerializeField] int _maxJumps = 2;
    [SerializeField] Transform _foot;
    [SerializeField] float _downPull = 0.1f;
    [SerializeField] float _fallTimer;
    [SerializeField] float _maxJumpDuration = 0.3f;
    [SerializeField] int _playerNumber = 1;

    private int _jumpsRemaining;
    private Rigidbody2D _rigidBody2D;
    private SpriteRenderer _sprite;
    private Animator _animator;
    private bool _isWalking;
    private float _jumpTimer;
    private bool _isJumping;

    Vector3 _startPosition;
    private float _horizontal;
    bool _isOnIceSurface;
    private bool _isGround;

    private void Awake()
    {
        _startPosition = transform.position;
        _rigidBody2D = GetComponent<Rigidbody2D>();
        _sprite = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
    }

    private void Update()
    {
        CalculateIsGround();
        
        if (_isOnIceSurface)
        {
            SliperyMove();
        }
        else
        {
            Move();
        }
        
        Animators();

        if (PressJump())
            Jump();

        if (HoldJupm())
            KeepJump();


        ResetJumpCountAndFallVelocity();
    }

    void ResetJumpCountAndFallVelocity()
    {
        if (_isGround && _fallTimer > 0)
        {
            _fallTimer = 0;
            _jumpsRemaining = _maxJumps;
        }
        else
        {
            _fallTimer += Time.deltaTime;
            var downForce = _downPull * _fallTimer * _fallTimer;
            _rigidBody2D.velocity = new Vector2(_rigidBody2D.velocity.x, _rigidBody2D.velocity.y - downForce);
        }
    }

    void Animators()
    {
        if (!_isWalking && Move() != 0)
        {
            _isWalking = true;
            _animator.SetBool("walk", _isWalking);
        }
        else if (_isWalking)
        {
            _isWalking = false;
            _animator.SetBool("walk", _isWalking);
        }

        if (_isWalking)
        {
            _sprite.flipX = Move() < 0;
        }
        
        _animator.SetBool("Jump", PressJump() && HoldJupm());
    }
    
    float Move()
    {
        _horizontal = Input.GetAxis($"P{_playerNumber}Horizontal");
        if (_horizontal != 0)
            _rigidBody2D.velocity = new Vector2(_horizontal * _speed, _rigidBody2D.velocity.y);
        return _horizontal;
    }

    void SliperyMove()
    {
            var desiredFlipMove = new Vector2(_horizontal * _speed, _rigidBody2D.velocity.y);
            var movementDestination =
                Vector2.Lerp(
                    _rigidBody2D.velocity, 
                    desiredFlipMove, 
                    Time.deltaTime * _slipFactor);
            _rigidBody2D.velocity = movementDestination;
    }
    void KeepJump()
    {
        _rigidBody2D.velocity = new Vector2(_rigidBody2D.velocity.x, _jumpVelocity);
        _fallTimer = 0;
        _jumpTimer += Time.deltaTime;
    }
    bool HoldJupm()
    {
        return Input.GetButton($"P{_playerNumber}Jump") && _jumpTimer < _maxJumpDuration;
    }

    void Jump()
    {
        _rigidBody2D.AddForce(Vector2.up * _jumpVelocity);
        _fallTimer = 0;
        _jumpTimer = 0;
        _jumpsRemaining--;
    }

    bool PressJump()
    {
        return Input.GetButtonDown($"P{_playerNumber}Jump") && _jumpsRemaining > 0;
    }

    void CalculateIsGround()
    {
        var hit = Physics2D.OverlapCircle(_foot.position, 0.1f, LayerMask.GetMask("Default"));
        _isGround = hit != null;
        
        if (hit != null)
            _isOnIceSurface = hit.CompareTag("SliperMove");
        else
            _isOnIceSurface = false;
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        var hit = Physics2D.OverlapCircle(_foot.position, 0.1f, LayerMask.GetMask("Default"));
        if (hit != null)
        {
            _jumpsRemaining = _maxJumps;
        }
    }

    internal void ResetToStart()
    {
        _rigidBody2D.position = _startPosition;
    }

    public void TeleportTo(Vector3 position)
    {
        _rigidBody2D.position = position;
        _rigidBody2D.velocity = Vector2.zero;
    }
}