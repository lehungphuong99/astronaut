using UnityEngine;

public class BreakableBrick : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.GetComponent<Player>() == null)
            return;
        if (collision.contacts[0].normal.y > 0)
            Break();
    }

    void Break()
    {
        var ParticleSystem = GetComponent<ParticleSystem>();
        ParticleSystem.Play();

        GetComponent<Collider2D>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;
    }
}
