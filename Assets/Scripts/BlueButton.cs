using UnityEngine;
using UnityEngine.Events;

public class BlueButton : MonoBehaviour
{
    [SerializeField] Sprite _pressedSprite;
    [SerializeField] UnityEvent _press;
    [SerializeField] UnityEvent _unPress;

    SpriteRenderer _spriteRenderer;
    Sprite _unPressedSprite;

    void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        
        _unPressedSprite = _spriteRenderer.sprite;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.GetComponent<Player>();
        if (player != null)
            return;

        PressButton();
        _spriteRenderer.sprite = _pressedSprite;
    }
    
    void OnTriggerExit2D(Collider2D collision)
    {
        var player = collision.GetComponent<Player>();
        if (player != null)
            return;
    
        _unPressedSprite = _spriteRenderer.sprite;
        _unPress?.Invoke();

    }

    void PressButton()
    {
        _press?.Invoke();
    }
}
