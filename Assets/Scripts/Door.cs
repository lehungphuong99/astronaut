using System;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

public class Door : MonoBehaviour
{
    [SerializeField] Sprite _doorOpenMid;
    [SerializeField] Sprite _doorOpenTop;
    [SerializeField] SpriteRenderer midDoor;
    [SerializeField] SpriteRenderer topDoor;
    [SerializeField] Door _transformPos;
    [SerializeField] Canvas _canvas;

    bool _open;
    
    [ContextMenu("OpenDoor")]

    void Open()
    {
        if (_canvas != null)
            _canvas.enabled = false;
        
        _open = true;
        midDoor.sprite = _doorOpenMid;
        topDoor.sprite = _doorOpenTop;
    }

    void Update()
    {
        if (_open = true && Coin._coinCollected >= 5)
        {
            Open();
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (_open == false)
            return;
        
        var player = GetComponent<Player>();
        if (player != null && _transformPos != null)
        {
            player.TeleportTo(_transformPos.transform.position);
        }
    }
}
