using UnityEngine;
using UnityEngine.UI;

public class CoinCollectedText : MonoBehaviour
{
    Text _text;

    void Start()
    {
        _text = GetComponent<Text>();
    }

    void Update()
    {
        _text.text = Coin._coinCollected.ToString();
    }
}
