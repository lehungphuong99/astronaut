using UnityEngine;

public class Coin : MonoBehaviour
{
    public static int _coinCollected;

    void OnTriggerEnter2D(Collider2D collider)
    {
        var player = collider.GetComponent<Player>();
        if (player == null)
            return;
        
        gameObject.SetActive(false);
        _coinCollected++;
        Debug.Log(_coinCollected);
    }
}