using UnityEngine;
using UnityEngine.Events;

public class KeyLock : MonoBehaviour
{

    [SerializeField] UnityEvent _UnLocking;
    public void Unlock()
    {
        Debug.Log("Unlocked");
        _UnLocking.Invoke();
    }
}
