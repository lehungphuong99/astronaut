using UnityEngine;
public class HorizontalCamera : MonoBehaviour
{
    [SerializeField] Transform _camera;
    void Update()
    {
        transform.position = new Vector3(_camera.position.x, transform.position.y, transform.position.z);
    }
}
