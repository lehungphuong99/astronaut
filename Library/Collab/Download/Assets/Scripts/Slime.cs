using UnityEngine;
public class Slime : MonoBehaviour
{

    [SerializeField] Transform _leftSense;
    [SerializeField] Transform _rightSense;
    [SerializeField] float _direction = -1;
    
    Rigidbody2D _rigidBody2D;

    void Start()
    {
        _rigidBody2D = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        _rigidBody2D.velocity = new Vector2(_direction, _rigidBody2D.velocity.y);

        if (_direction < 0)
        {
            sensor(_leftSense);
        }

        else
        {
            sensor(_rightSense);
        }
    }

    private void sensor(Transform sense)
    {
        Debug.DrawRay(sense.position, Vector2.down * 1f, Color.green);
        var leftSensehit = Physics2D.Raycast(sense.position, Vector2.down, 0.1f
        );
        if (leftSensehit.collider == null)
            Turnaround();
        
        Debug.DrawRay(sense.position, new Vector2(_direction, 0) * 1f, Color.green);
        var rightSensehit = Physics2D.Raycast(sense.position, new Vector2(_direction, 0), 0.1f
        );
        if (rightSensehit.collider != null)
            Turnaround();
    }

    void Turnaround()
    {
        _direction *= -1;
        var spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.flipX = _direction > 0;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        var player = collision.collider.GetComponent<Player>();
        if (player != null)
            player.ResetToStart();
    }
}
